import styled from 'styled-components';

const StyleFooter = styled.div`
  .Main-Footer {
    background-color: black;
    color: white;
    position: absolute;
    bottom: 0;
    right: 0;
    padding-right: 5%;
  }
`;

export default StyleFooter;
